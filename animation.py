import glob
import random

import pygame
from pygame import *

from macro import *
import settings

class Animation(pygame.sprite.Sprite):
    def __init__(self, id, loop=True, speed=2, size_multiplier=None, tiled=False):
        self.frames = [
            load_sprite(frame[:-4], multiplier=size_multiplier, convert=False)
            for frame in sorted(glob.glob("./assets/sprites/" + id + "*.png"))
        ]

        if tiled:
            coords = (random.randint(0, 2) * settings.TILE_S,
                      random.randint(0, 2) * settings.TILE_S)
            self.frames = [
                self.get_tilesheet_tile(
                    frame,
                    coords
                )
                for frame in self.frames
            ]

        self.frame = 0
        self.counter = 0

        self.loop = loop
        self.done = False
        self.speed = speed

    def get_tilesheet_tile(self, tilesheet, coords):
        tile = Surface(settings.tile_size)

        x, y = coords
        tile.blit(tilesheet, (-x, -y))
        tile = tile.convert()

        return tile

    def next(self, angle):
        """Return next frame."""
        if self.loop or self.frame < len(self.frames) -1:
            self.advance()
        else:
            self.done = True

        return pygame.transform.rotate(self.frames[self.frame].copy(), angle-90)

    def advance(self):
        """Advance frame count without returning."""
        if self.counter % self.speed == 0:
            self.frame += 1

            if self.frame > len(self.frames) -1:
                self.frame = 0
                self.counter = 0

        self.counter += 1

    def reset(self):
        """Reset animation to first frame."""
        self.frame = 0
        self.counter = 0
