import pygame
from pygame import *
from settings import *

class Camera:
    def __init__(self):
        w, h = MAP_SIZE
        self.position = Rect(0, 0, w, h)

    def apply(self, target):
        """Move target sprite so that is looks like we moved the viewport."""
        
        if(type(target) is Rect):
            mtarget = target.move(self.position.topleft)
        else:
            mtarget = target.rect.move(self.position.topleft)
        
        return mtarget

    def update(self, player):
        """Calculate how much objects need to be moved."""
        w, h = MAP_SIZE
        dw, dh = DISPLAY

        # Get player position
        x, y = player.rect.bottomleft

        # Get camera position where player is in center
        x, y = x - dw/2, y - dh/2

        """Make sure camera position doesn't go beyond boundaries."""

        # Left edge
        if x < 0:
            x = 0

        # Right edge
        if x > w - dw:
            x = w - dw

        # Up edge
        if y < 0:
            y = 0

        # Down edge
        if y > h - dh:
            y = h - dh

        # Invert x and y in order to calculate
        # how much to shift the opposite way
        self.position = Rect(-x, -y, w, h)
