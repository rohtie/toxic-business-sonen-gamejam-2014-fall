import random, math, pygame
from pygame import Surface, Color, Rect, sprite

from macro import load_sprite
from mixins import renderMixin, AnimMixin
from animation import Animation
import settings


class GenericObject(pygame.sprite.Sprite):
    def __init__(self, coords, size):
        super(GenericObject, self).__init__()

        self.image = None

        # Coordinates, size
        x, y = coords
        w, h = size
        self.rect = Rect( x, y, w, h)

    def __repr__(self):
        return unicode(self.rect.topleft)

    def move(self, objs):
        pass

    def check_collisions(self, objs, landscape, no_trees, game):
        pass

    def die(self, objs):
        if self in objs:
            objs.remove(self)

    def generate_tile(self):
        self.image = self.get_tilesheet_tile(
            self.tilesheet,
            (random.randint(0, 2) * settings.TILE_S,
             random.randint(0, 2) * settings.TILE_S)
    )

    def get_tilesheet_tile(self, tilesheet, coords):
        tile = Surface(settings.tile_size)

        x, y = coords
        tile.blit(tilesheet, (-x, -y))
        tile = tile.convert()

        return tile

    @staticmethod
    def spawn_flame(objs,pos, speed, angle):
        speed_n = [0,0]
        speed_n[0] = speed[0] * settings.FLAME_SPEED
        speed_n[1] = speed[1] * settings.FLAME_SPEED
        objs.append(Flame(pos, speed_n, angle))

    @staticmethod
    def spawn_tree(objs,landscape,no_trees,pos):
        objs.append(Tree(pos))
        no_trees.remove(landscape[int(pos[0] / settings.TILE_S)][int(pos[1] / settings.TILE_S)] )
        landscape[int(pos[0] / settings.TILE_S)][int(pos[1] / settings.TILE_S)].state = True
        landscape[int(pos[0] / settings.TILE_S)][int(pos[1] / settings.TILE_S)].delay = 0.0
        landscape[int(pos[0] / settings.TILE_S)][int(pos[1] / settings.TILE_S)].notify_neighbours(landscape)

    @staticmethod
    def spawn_burning_tree(objs,landscape,no_trees,pos):
        objs.append(BurningTree(pos, landscape, no_trees))

    @staticmethod
    def spawn_success_zone(objs,pos):
        objs.append(SuccessZone(pos))


class Flame(GenericObject, AnimMixin):
    def __init__(self, coords, speed, angle):
        super(Flame,self).__init__(coords, settings.FLAME_SIZE)
        # Image
        # flame = Surface(settings.tile_size)
        # flame = flame.convert()
        # flame.fill(Color("red"))
        # self.image = flame
        self.angle = angle
        self.anim = Animation('flame', size_multiplier=1, speed=5)

        # "Physics"
        # Speed = number of pixels per frame
        self.speed = speed
        self.distance = [0,0]

    def move(self, objs):

        if(math.sqrt(self.distance[0]**2 + self.distance[1]**2) > settings.FLAME_MAX_DISTANCE):
            self.die(objs)

        # Move
        self.rect.x += self.speed[0]
        self.rect.y += self.speed[1]

        self.distance[0] += self.speed[0]
        self.distance[1] += self.speed[1]

        # Die
        if( self.rect.x < 0 or self.rect.x >= settings.num_tiles[0]*settings.tile_size[0] ):
            self.die(objs)
        elif(self.rect.y < 0 or self.rect.y >= settings.num_tiles[1]*settings.tile_size[0] ):
            self.die(objs)

    def check_collisions(self, objs, landscape, no_trees, game):
        # Iterate over all objects
        for obj in objs:
            if(type(obj) == Tree and obj != self):
                if (sprite.collide_rect(self,obj)):
                    self.die(objs)
                    obj.kindle(objs, landscape, no_trees)

class Tree(GenericObject, renderMixin):
    def __init__(self, coords):
        super(Tree,self).__init__(coords, settings.tile_size)
        # Image
        tree = Surface(settings.tile_size)
        tree = tree.convert()
        #tree.fill(Color("#1A7A06"))
        #self.image = tree

        self.tilesheet = load_sprite('assets/tiles/forest_sheet')
        self.generate_tile()

    def kindle(self, objs, landscape, no_trees):
        #spawn burning tree
        # print "Spawn burning tree"
        self.spawn_burning_tree(objs, landscape, no_trees, (self.rect.x, self.rect.y))
        self.die(objs)


class BurningTree(GenericObject, AnimMixin):
    def __init__(self, coords, landscape, no_trees):
        super(BurningTree,self).__init__(coords, settings.tile_size)

        # Animation
        self.anim = Animation('forest_burning_sheet', tiled=True)
        self.angle = 0

        # Logic
        self.lifetime = settings.BURN_TIME

        # Keeper vars
        self.landscape = landscape
        self.no_trees = no_trees


    def move(self,objs):
        if(self.lifetime > 0):
            self.lifetime -= 1
        else:
            self.die(objs)

    def die(self, objs):
        # Mark grass free
        self.landscape[int(self.rect.x/ settings.TILE_S)][int(self.rect.y/settings.TILE_S)].state = False
        self.landscape[int(self.rect.x/ settings.TILE_S)][int(self.rect.y/settings.TILE_S)].delay = 0.0
        self.landscape[int(self.rect.x/ settings.TILE_S)][int(self.rect.y/settings.TILE_S)].notify_neighbours(self.landscape)
        self.no_trees.append(self.landscape[int(self.rect.x/ settings.TILE_S)][int(self.rect.y/settings.TILE_S)])

        # Remove from objects
        if self in objs:
            objs.remove(self)


class SuccessZone(GenericObject, renderMixin):
    def __init__(self, coords):
        super(SuccessZone,self).__init__(coords, (settings.tile_size[0]*6,settings.tile_size[0]*6 ))
        # Image
        tree = Surface((settings.tile_size[0]*6,settings.tile_size[0]*6 ))
        tree = tree.convert()
        tree.fill(Color("blue"))
        self.image = None

        #self.tilesheet = load_sprite('assets/tiles/forest_sheet')
        #self.generate_tile()
    def check_collisions(self, objs, landscape, no_trees, game):
        # Iterate over all objects
        for obj in objs:
            if(hasattr(obj,"pipeline") and obj != self):
                if (sprite.collide_rect(self,obj)):
                    # Game success!
                    game.fail = False
                    game.running = False

