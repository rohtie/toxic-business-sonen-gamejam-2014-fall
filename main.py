import pygame
from pygame import Surface, Color, time

from game import Game
from macro import play_music, load_sprite
import settings

# Init pygame
pygame.init()
pygame.mixer.init()
pygame.display.set_caption("Toxic Business")
timer = pygame.time.Clock()

# Screen is the main surface, which will be shown
screen = pygame.display.set_mode(
    settings.DISPLAY,
    # pygame.FULLSCREEN | pygame.HWSURFACE
)

# Load black background
background = Surface(settings.DISPLAY)
background = background.convert()
background.fill(Color('#000000'))

timer = pygame.time.Clock()


def main():
    """--- INTRO ---"""
    play_music("DST-HonorGuard")

    current_slide = 0
    intro = [
        load_sprite('assets/story/cut001'),
        load_sprite('assets/story/cut002'),
    ]

    """--- OUTRO ---"""
    outro = [
        load_sprite('assets/story/fail'),
        load_sprite('assets/story/success'),
    ]


    intro_running = settings.intro
    while intro_running:
        timer.tick(60)

        screen.blit(intro[current_slide], (0, 0))

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if current_slide == len(intro) - 1:
                        intro_running = False
                    else:
                        current_slide += 1

            handle_quit(event)

        pygame.display.update()
    current_slid = 0
    """---- GAME ----"""

    # Game music
    play_music("DST-AngryMod")
    # playMusic("DST-DarkSeraphim")
    while True:

        game = Game()
        while game.running:
            timer.tick(settings.FPS)  # 60 fps
            pygame.display.set_caption("Toxic Business " + str(timer.get_fps()))

            screen.blit(background, (0, 0))
            game.update()

            game.render(screen)

            game.do_coll()

            for event in pygame.event.get():
                game.handle_event(event)
                handle_quit(event)

            pygame.display.update()

        # Show
        time.delay(1000)

        # OUTRO
        timer.tick(60)
        out_running = True
        while out_running:
            if(game.fail):
                screen.blit(outro[0], (0, 0))
            else:
                screen.blit(outro[1], (0, 0))


            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        out_running = False
                handle_quit(event)

            pygame.display.update()


def handle_quit(event):
    if event.type == pygame.QUIT:
        raise(SystemExit, 'QUIT')

    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
            raise(SystemExit, 'Quit')

main()
