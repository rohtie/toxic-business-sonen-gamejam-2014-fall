import pygame

class renderMixin:
    def render(self, surface, camera):
        if(self.image):
            surface.blit(self.image, camera.apply(self))

class AnimMixin:
    def render(self, surface, camera):
        if hasattr(self, 'show_idle') and self.show_idle:
            rot_image = pygame.transform.rotate(self.idle_image.copy(), self.angle-90)
            rot_rect = rot_image.get_rect(center=self.rect.center)
            surface.blit(rot_image, camera.apply(rot_rect))

        else:
            rot_image = self.anim.next(self.angle)
            rot_rect = rot_image.get_rect(center=self.rect.center)
            surface.blit(rot_image, camera.apply(rot_rect))
