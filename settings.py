DISPLAY = (1280, 720)

FPS = 60

TILE_MULTIPLIER = 2
TILE_S = 16 * TILE_MULTIPLIER
tile_size = (TILE_S, TILE_S)
num_tiles = (100, 60)
MAP_SIZE = (100 * TILE_S, 60 * TILE_S)

PLAYER_SIZE = (32, 32)
PLAYER_ACCEL = 4
PLAYER_MAX_SPEED = 8
# In percent
PLAYER_FRICTION_BASE = 0.4

PCHECK = FPS * 0.5

FLAME_RATE = FPS * 0.0001
FLAME_MAX_DISTANCE = 200 * TILE_MULTIPLIER
FLAME_SPEED = 64

# Object sizes
FLAME_SIZE = (32, 32)
TREE_SIZE = (16, 16)

# Growth
tree_growth = FPS * 15
tree_growth_rate = 10
water_growth_rate = 1

# Burn time, seconds
BURN_TIME = FPS * 25 * 0.02

PIPE_DELAY = 0

music = True
intro = True
