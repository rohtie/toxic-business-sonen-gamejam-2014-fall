import pygame, math
from math import cos, sin
from pygame import Surface, Color, Rect, mouse, sprite
from pygame.locals import *

import settings
from settings import *
from mixins import renderMixin, AnimMixin
from objects import GenericObject, Tree
from animation import Animation
from macro import load_sprite

class Player(GenericObject, AnimMixin):
    def __init__(self, coords):
        super(Player,self).__init__(coords, settings.PLAYER_SIZE)

        self.angle = -90
        self.anim = Animation('player_run', size_multiplier=1)
        self.idle_image = load_sprite('player_idle', multiplier=1, convert=False)
        self.show_idle = True

        # self.tilesheet = load_sprite('assets/tiles/forest_sheet')
        # self.generate_tile()

        self.velocity = [0,0]

        self.x_vel = 0
        self.y_vel = 0
        
        self.pcheck = 0

        x, y = coords
        w, h = settings.PLAYER_SIZE
        self.rect = Rect(x, y, w, h)

        # Control states
        self.up = False
        self.down = False
        self.left = False
        self.right = False

        self.shooting = False
        self.flame_ticks = 0

    def handle_input(self, event, camera):
        # Mouse
        if event.type == MOUSEMOTION:
            # Adjust for camera movement
            self.mouse_pos = mouse.get_pos()
            # rect_tmp = Rect(self.mouse_pos, (0,0))
            # self.mouse_pos = camera.apply(rect_tmp).topleft

            rect_tmp = Rect((self.rect.x, self.rect.y), (0,0))
            rect_tmp = camera.apply(rect_tmp)
            px = rect_tmp.x
            py = rect_tmp.y

            # dx = self.mouse_pos[0] - self.rect.x  +.01
            # dy = self.rect.y - self.mouse_pos[1] +.01
            dx = self.mouse_pos[0] - px  +.01
            dy = py - self.mouse_pos[1] +.01

            # 1. quadrant
            if(dx > 0 and dy > 0):
                self.angle = math.degrees(math.atan( dy / dx ))
            # 2. quadrant
            elif(dx < 0 and dy > 0):
                self.angle = - math.degrees(math.atan( dx / dy )) + 90
            # 3. quadrant
            elif(dx < 0 and dy < 0):
                self.angle = math.degrees(math.atan( dy / dx )) + 180
            # 4. quadrant
            elif(dx > 0 and dy < 0):
                self.angle = - math.degrees(math.atan( dx / dy )) + 270
            else:
                self.angle = math.degrees(math.atan( dy / dx ))

            # print "Angle: %f" % (self.angle)

        if event.type == MOUSEBUTTONDOWN:
            pos = mouse.get_pos()
            self.shooting = True

        if event.type == MOUSEBUTTONUP:
            pos = mouse.get_pos()
            self.shooting = False

        # Keyboard
        if event.type == KEYDOWN:
            # Forwards
            if event.key == K_w:
                self.up = True
            # Backwards
            if event.key == K_s:
                self.down = True
            # Strafe left
            if event.key == K_a:
                self.left = True
            # Strafe right
            if event.key == K_d:
                self.right = True
            # Fire
            if event.key == K_SPACE:
                self.shooting = True

        if event.type == KEYUP:
            # Forwards
            if event.key == K_w:
                self.up = False
            # Backwards
            if event.key == K_s:
                self.down = False
            # Strafe left
            if event.key == K_a:
                self.left = False
            # Strafe right
            if event.key == K_d:
                self.right = False
            # Fire
            if event.key == K_SPACE:
                self.shooting = False


    def update_velocity(self):
        push = [ 0, 0]
        if(self.up):
            push[0] = settings.PLAYER_ACCEL + settings.PLAYER_FRICTION_BASE
        if(self.down):
            push[0] = - settings.PLAYER_ACCEL + settings.PLAYER_FRICTION_BASE
        if(self.left):
            push[1] =  settings.PLAYER_ACCEL + settings.PLAYER_FRICTION_BASE
        if(self.right):
            push[1] = -settings.PLAYER_ACCEL + settings.PLAYER_FRICTION_BASE

        if(push[0] == 0 and push[1] == 0):
            return

        # Update velocity
        push = self.vec_rot(push,math.radians(self.angle))

        if(self.vec_length(self.velocity) > settings.PLAYER_MAX_SPEED):
            push = self.vec_norm(push)
        self.velocity = self.vec_add(self.velocity, push)


    def move(self, objs):
        self.update_velocity()

        # print "\nMove in direction %f,%f" % (self.velocity[0],self.velocity[1])
        # print "Pos: %d,%d, new : %f, %f" % (self.rect.x,self.rect.y, self.rect.x + self.velocity[0], self.rect.y + self.velocity[1])
        # Apply friction
        if(self.velocity[0] != 0):
            self.velocity = self.vec_add(self.velocity, self.get_friction_vec(self.velocity))
        if(self.velocity[1] != 0):
            self.velocity = self.vec_add(self.velocity, self.get_friction_vec(self.velocity))

        # print "New Pos %f, %f" % (self.rect.x,self.rect.y)
        # Shoot?
        if(self.shooting and self.flame_ticks <= 0):
            GenericObject.spawn_flame(objs, (self.rect.x, self.rect.y), self.get_offsets(self.angle), self.angle)
            self.flame_ticks = settings.FLAME_RATE
        else:
            if(self.flame_ticks > 0):
                self.flame_ticks -= 1
        
        self.show_idle = (round(self.velocity[0]) == 0 or round(self.velocity[1]) == 0)
        
        # Distance to move
        # Move at all?
        # Iterate over all objects
        if(self.pcheck >= 0):
            self.pcheck = settings.PCHECK
            for obj in objs:
                if(type(obj) is Tree and obj != self):
                    #if(obj.rect.collidepoint(self.rect.x,self.rect.y)):
                    #    return
                    if (sprite.collide_rect(self,obj)):
                        return
        else:
            self.pcheck -= 1
                    
        if(self.velocity[0] != 0):
            if(self.rect.x + self.velocity[0] > 0 and self.rect.x + self.velocity[0] < settings.num_tiles[0]*settings.tile_size[0]):
                self.rect.x += round(self.velocity[0])
        if(self.velocity[1] != 0):
            if(self.rect.y + self.velocity[1] > 0 and self.rect.y + self.velocity[1] < settings.num_tiles[1]*settings.tile_size[1]):
                self.rect.y += round(self.velocity[1])

        
        
        
        

    def vec_length(self,vector):
        return math.sqrt( vector[0]**2 + vector[1]**2)

    def vec_add(self,vec1, vec2):
        # print "\nAdding %f,%f to %f,%f" % (vec1[0], vec1[1], vec2[0], vec2[1])
        test = [sum(x) for x in zip(vec1, vec2)]
        # print "Got: %f,%f" % (test[0], test[1])
        return [sum(x) for x in zip(vec1, vec2)]

    def vec_rot(self,vec1, angle):
        vx = vec1[0]
        vy = vec1[1]

        # print "\nRot %f,%f to angle: %f" % (vec1[0],vec1[1], math.degrees(angle))
        x = vx * cos(angle) - vy*sin(angle)
        y = - (vx * sin(angle) + vy*cos(angle))

        # print "Rotated: %f,%f " % (x,y)
        return [x,y]

    def vec_norm(self,vec):
        vec_out = [0,0]
        l = self.vec_length(vec)
        if(l <= 0):
            return vec
        vec_out[0] = vec[0] / l
        vec_out[1] = vec[1] / l

        return vec_out

    def get_friction_vec(self,velocity):
        friction = [-self.velocity[0], -self.velocity[1]]
        #friction = self.vec_norm(friction)
        friction[0] = friction[0]/2*settings.PLAYER_FRICTION_BASE
        friction[1] = friction[1]/2*settings.PLAYER_FRICTION_BASE

        #if(friction[0] > 1 or friction[0] < -1 ):
        #    friction[0] = friction[0]/2*settings.PLAYER_FRICTION_BASE
        #else:
        #    friction[0] = 0
        #if(friction[1] > 1 or friction[1] < -1 ):
        #    friction[1] = friction[1]*settings.PLAYER_FRICTION_BASE
        #else:
        #    friction[1] = 0

        return friction

    @staticmethod
    def get_offsets(angle):
        # print "Angle: %d" % (angle)
        y = 1 * math.cos(math.radians(angle)+1.57)
        x = 1 * math.sin(math.radians(angle)+1.57)
        return (x,y)




