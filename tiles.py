import random

import pygame
from pygame import Surface, Color, Rect

from macro import load_sprite
from objects import GenericObject, Flame, Tree
from mixins import renderMixin
import settings


class GenericTile(pygame.sprite.Sprite, renderMixin):
    def __init__(self, coords):
        super(GenericTile, self).__init__()

        self.image = None
        self.tilesheet = None

        self.x, self.y = coords
        w, h = settings.tile_size
        self.rect = Rect(self.x, self.y, w, h)

        self.x = self.x / settings.TILE_S
        self.y = self.y / settings.TILE_S

        #x, y = coords
        #w, h = settings.tile_size
        #self.rect = Rect(x, y, w, h)
        self.waters = 0
        self.neighbours = 0

    def get_tilesheet_tile(self, tilesheet, coords):
        tile = Surface(settings.tile_size)

        x, y = coords
        tile.blit(tilesheet, (-x, -y))
        tile = tile.convert()

        return tile

    def generate_tile(self):
        self.image = self.get_tilesheet_tile(
            self.tilesheet,
            (random.randint(0, 2) * settings.TILE_S,
             random.randint(0, 2) * settings.TILE_S)
    )


    def __repr__(self):
        return unicode(self.rect.topleft)


class Grass(GenericTile):
    def __init__(self, coords):
        super(Grass,self).__init__(coords)

        grass = Surface(settings.tile_size)
        grass = grass.convert()
        grass.fill(Color("green"))

        self.image = grass
        self.tilesheet = load_sprite('assets/tiles/grass_sheet')
        self.generate_tile()

        # State of forestation
        self.lock  = False
        self.state = False
        self.delay = 0.0

    def __repr__(self):
        return (unicode(self.rect.topleft), "Grass")

    def update_state(self, landscape, no_trees, objs):
        # Can I haz tree?
        if(self.lock):
            return

        # If no trees, dont change anything
        if(self.neighbours == 0):
            return
        if(self.state == True):
            return

        if(self.delay >= settings.tree_growth):
            # self.state = True
            # self.delay = 0.0
            #
            # self.notify_neighbours(landscape)

            # FIX later with proper trees
            # Spawn forest object here
            # self.image = forest
            GenericObject.spawn_tree(objs,landscape, no_trees,(self.rect.x, self.rect.y) )

        else:
            # Follow water
            if(self.waters > 0):
                self.delay += (settings.water_growth_rate)* (self.waters / 8.0) * settings.tree_growth/settings.FPS + (settings.tree_growth_rate) * (self.neighbours / 8.0)  * settings.tree_growth/settings.FPS
            else:
                self.delay += (settings.tree_growth_rate) * ((self.neighbours/ 8.0) )

            # Exponential
            # self.delay  += 1 * (1+(self.neighbours / 8.0)) ** self.n
            # Linear
            # self.delay += 1 * (1 + self.waters / 8)
        return

    def notify_neighbours(self, landscape):
        radius = 1
        x_start = self.x - radius
        y_start = self.y - radius
        x_stop  = self.x + radius
        y_stop  = self.y + radius

        for x in range(int(x_start), int(x_stop + 1)):
            for y in range(int(y_start), int(y_stop + 1)):
                if(x >= 0 and x <= 99  and y >= 0 and y <= 59 and (x != self.x or y != self.y)):
                    # A tree was added
                    if(self.state):
                        landscape[x][y].neighbours += 1
                    # A tree was removed
                    else:
                        landscape[x][y].neighbours -= 1

class Water(GenericTile):
    def __init__(self, coords):
        super(Water, self).__init__(coords)

        water = Surface(settings.tile_size)
        water = water.convert()
        water.fill(Color("blue"))

        self.image = water
        self.tilesheet = load_sprite('assets/tiles/water_sheet')
        self.generate_tile()



    def update_state(self, landscape):
        pass

    def notify_neighbours(self, landscape):
        radius = 1
        x_start = self.x - radius
        y_start = self.y - radius
        x_stop  = self.x + radius
        y_stop  = self.y + radius

        for x in range(int(x_start), int(x_stop + 1)):
            for y in range(int(y_start), int(y_stop + 1)):
                if(x >= 0 and x <= 99  and y >= 0 and y <= 59 and (x != self.x or y != self.y)):
                    # if(type(landscape[x][y]) is Grass):
                    # All types of tiles want to know about water
                    landscape[x][y].waters += 1
