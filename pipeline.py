import pygame
from pygame import *

from settings import *
from mixins import renderMixin, AnimMixin
from objects import GenericObject
from macro import load_sprite
from tiles import Water

RIGHT = 0
LEFT = 1
UP = 2
DOWN = 3

DIRECTION_TO_COORDS = {
    RIGHT: [(RIGHT, 4,  0), (DOWN,  3,  2), (UP,    3, -3)],
    DOWN:  [(DOWN,  0,  4), (RIGHT, 2,  3), (LEFT, -3,  3)],
    LEFT:  [(LEFT, -4,  0), (UP,   -2, -3), (DOWN, -2,  2)],
    UP:    [(UP,    0, -4), (RIGHT, 2, -2), (LEFT, -3, -2)],
}


class PipelineCorner(GenericObject, renderMixin):
    def __init__(self, coords, source, dest):
        image = load_sprite('pipeline_corner')
        x, y = coords

        if source == RIGHT:
            coords = (x + 3*TILE_S, y)

        if source == DOWN:
            coords = (x, y + 3*TILE_S)

        if source == LEFT:
            coords = (x - 2*TILE_S, y)

        if source == UP:
            coords = (x, y - 2*TILE_S)

        if source == RIGHT and dest == DOWN:
            # correct
            pass

        if source == RIGHT and dest == UP:
            image = self.rotate(image, -90)
            # 90 deg

        if source == LEFT and dest == DOWN:
            image = self.rotate(image, 90)
            # -90 deg

        if source == LEFT and dest == UP:
            image = self.rotate(image, -180)
            # -180 deg

        if source == UP and dest == LEFT:
            # correct
            pass

        if source == UP and dest == RIGHT:
            image = self.rotate(image, 90)
            # 90 deg

        if source == DOWN and dest == LEFT:
            image = self.rotate(image, -90)
            # -90 deg

        if source == DOWN and dest == RIGHT:
            image = self.rotate(image, -180)
            # -180 deg

        super(PipelineCorner, self).__init__(coords, image.get_size())
        self.image = image
        self.coords = coords

    def rotate(self, image, angle):
        return pygame.transform.rotate(image, angle)

    def dont_grow(self, landscape, no_trees):
        x, y = self.coords
        x, y = (x/TILE_S, y/TILE_S)
        if(landscape[int(x + 1)][int(y + 1)] in no_trees):
            no_trees.remove(landscape[int(x + 1)][int(y + 1)])


class Pipeline(GenericObject, renderMixin):
    def __init__(self, coords, direction, landscape, no_trees, objs, game):
        image = load_sprite('pipeline')

        if direction in (UP, DOWN):
            image = pygame.transform.rotate(image, 90)

        super(Pipeline,self).__init__(coords, image.get_size())
        self.image = image

        self.direction = direction
        self.out_of_world = False

        self.pipeline = True

        self.has_space = True
        x_coor = self.rect.x / TILE_S
        y_coor = self.rect.y / TILE_S
        w, h = self.image.get_size()

        #print "Checking range %d,%d" % (w / TILE_S, h / TILE_S)
        states = []

        # Check collision
        # Iterate over all objects
        for obj in objs:
            if(hasattr(obj,"pipeline") and obj != self):
                if(obj.rect.collidepoint(self.rect.center)):
                    # Game success!
                    game.fail = True
                    game.running = False

        for x in range(int(w / TILE_S)):
            for y in range(int(h / TILE_S)):
                # Out of world?
                if(x+x_coor < 0 or x+x_coor > num_tiles[0]):
                    self.out_of_world = True
                    self.has_space = False
                    return
                if(y+y_coor < 0 or y+y_coor >   num_tiles[1]):
                    self.out_of_world = True
                    self.has_space = False
                    return

                if (isinstance(landscape[int(x+x_coor)][int(y+y_coor)], Water)):
                    game.fail = False
                    game.running = False
                    #self.has_space = False

                # elif (landscape[int(x+x_coor)][int(y+y_coor)].lock):
                #     self.has_space = False

                elif(landscape[int(x+x_coor)][int(y+y_coor)].state):
                        self.has_space = False

            #        break
            #else:
            #    break


        # Mark tiles occupied
        # Check every tile

        # Top left / First tile index for this pipeline
        if self.has_space:
            for x in range(int(w / TILE_S)):
                for y in range(int(h / TILE_S)):

                    landscape[int(x+x_coor)][int(y+y_coor)].lock = True

                    if (landscape[int(x+x_coor)][int(y+y_coor)] in no_trees):
                        no_trees.remove(landscape[int(x+x_coor)][int(y+y_coor)])


    def place_next(self, landscape, no_trees, game, objs):
        """Place a new pipeline if enough space
        to the left, right or forward."""

        x, y = self.rect.topleft
        tmp_pipeline = None

        out_of_world = True

        for pipeline in DIRECTION_TO_COORDS[self.direction]:
            direction, xo, yo = pipeline

            tmp_pipeline = Pipeline(
                (x + (xo * TILE_S), y + (yo * TILE_S)),
                direction,
                landscape,
                no_trees,
                objs,
                game
            )

            if tmp_pipeline.has_space:
                return tmp_pipeline

            out_of_world = tmp_pipeline.out_of_world

        # Game over
        if (out_of_world):
            # Game over
            game.fail = True
            game.running = False

        return None
