from pygame import Surface, Color, event, mouse
from pygame.locals import *

from camera import Camera
from player import Player
from pipeline import Pipeline, PipelineCorner
from tiles import GenericTile, Grass, Water
from objects import GenericObject, Flame, Tree
from mapgenerator import MapGenerator
import settings

class Game:
    def __init__(self):
        self.camera = Camera()
        #self.player = Player((0, 0))
        self.player = Player((1*settings.TILE_S, 1*settings.TILE_S))

        self.try_pipe = settings.PIPE_DELAY
        self.running = True
        self.maps = MapGenerator('002')

        # Win fail
        self.fail = True

        # TMP forest image cache
        forest = Surface(settings.tile_size)
        forest = forest.convert()
        forest.fill(Color("yellow"))

        # Landscape Data
        self.landscape = self.maps.landscape_map
        self.water_notify_all()

        self.no_trees = []

        for x in range(settings.num_tiles[0]):
            for y in range(settings.num_tiles[1]):

                if(type(self.landscape[x][y]) is Grass):

                    if(self.landscape[x][y].state == False ):
                        self.no_trees.append(self.landscape[x][y])
                        # print "TYPE"
                        # print self.no_trees




        # Objects
        self.objs = []



        # First pipeline
        self.current_pipeline = Pipeline((0,  settings.TILE_S*3), 0, self.landscape, self.no_trees, self.objs, self)
        self.objs.append(self.current_pipeline)

        # GenericObject.spawn_success_zone(self.objs,(70*settings.TILE_S,32*settings.TILE_S ))
        GenericObject.spawn_success_zone(self.objs,(57*settings.TILE_S,4*settings.TILE_S ))

        self.maps.generate_trees(self.objs, self.landscape, self.no_trees)

    def water_notify_all(self):
      """ Notify all tiles surrounding water of its presence """
      tiles_x, tiles_y = settings.num_tiles
      for x in range(tiles_x):
        for y in range(tiles_y):
          if(type(self.landscape[x][y]) is Water):
            self.landscape[x][y].notify_neighbours(self.landscape)


    def handle_event(self, event):
        """Handle input events."""
        self.player.handle_input(event, self.camera)

    def update(self):
        """Update game stuff."""

        if(self.try_pipe <= 0):
            next_pipeline = self.current_pipeline.place_next(self.landscape, self.no_trees, self, self.objs)
            if next_pipeline:
                if (next_pipeline.direction !=
                        self.current_pipeline.direction):
                    corner = PipelineCorner(
                            self.current_pipeline.rect.topleft,
                            self.current_pipeline.direction,
                            next_pipeline.direction
                        )
                    corner.dont_grow(self.landscape, self.no_trees)
                    self.objs.append(corner)

                self.current_pipeline = next_pipeline
                self.objs.append(self.current_pipeline)

            self.try_pipe = settings.PIPE_DELAY
        else:
            self.try_pipe -= 1

        # landscape_snap = copy.deepcopy(landscape)
        # print "UPDATE!!!"
        # Transitions
        #tiles_x, tiles_y = settings.num_tiles
        #for y in range(tiles_y):
        #    for x in range(tiles_x):
        #        if(type(self.landscape[x][y]) is Grass):
        #            self.landscape[x][y].update_state(self.landscape, self.objs)
        for no_tree in self.no_trees:
            no_tree.update_state(self.landscape, self.no_trees,self.objs)

        # Update positions
        self.camera.update(self.player)

        for obj in self.objs:
            if(type(obj) is not Tree):
                obj.move(self.objs)
            # obj.check_collisions(self.objs)

        self.player.move(self.objs)


    def do_coll(self):
        for obj in self.objs:
            if(type(obj) is not Tree):
                obj.check_collisions(self.objs, self.landscape, self.no_trees, self)

        self.player.check_collisions(self.objs, self.landscape, self.no_trees, self)


    def render(self, screen):
        """Render stuff."""
        # Render landscape
        tiles_x, tiles_y = settings.num_tiles
        for y in range(tiles_y):
            for x in range(tiles_x):
                #tile = self.landscape[x][y]
                #if tile:
                # Only blit grass that is not ocluded
                if(type(self.landscape[x][y]) is Grass):
                    if(self.landscape[x][y].state != True):
                        self.landscape[x][y].render(screen, self.camera)
                else:
                    self.landscape[x][y].render(screen, self.camera)

        # Render objects
        for obj in self.objs:
            obj.render(screen, self.camera)

        self.player.render(screen, self.camera)
