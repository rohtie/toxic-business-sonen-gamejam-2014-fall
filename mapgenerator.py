from PIL import Image

from tiles import Grass, Water
from objects import GenericObject
import settings

index_to_object = {
    'tree': {
        # TODO: Spawn correct tiles
        11: 'Tree',
    },
    'landscape': {
        # TODO: Spawn correct tiles
        11: Grass,
        16: Water,
        # 4:  'water_border',
        # 12: 'swamp',
        # 13: 'swamp_border',
        # 22: 'stone',
        # 20: 'stone_border',
    },
}


class MapGenerator:
    def __init__(self, map_id):
        self.map_id = map_id
        self.landscape_map = self.generate_map('landscape')
        # self.tree_map = self.generate_map('tree')

    def generate_map(self, type):
        """Generate map based on type."""
        data = self.load_map_data(type, self.map_id)

        if not data:
            return None

        generated_map = [[None for y in range(60)] for x in range(100)]

        tw, th = settings.tile_size

        tiles_x, tiles_y = settings.num_tiles
        for y in range(tiles_y):
            for x in range(tiles_x):
                generated_map[x][y] = (
                    index_to_object[type][data[x, y]]((x * tw, y * th))
                    if data[x, y] in index_to_object[type] else Grass((x * tw, y * th))
                )

        return generated_map

    def generate_trees(self, objs, landscape, no_trees):
        data = self.load_map_data('tree', self.map_id)

        if not data:
            return None

        tiles_x, tiles_y = settings.num_tiles
        for y in range(tiles_y):
            for x in range(tiles_x):
                if data[x,y] == 11:
                    GenericObject.spawn_tree(
                        objs,
                        landscape,
                        no_trees,
                        (x*settings.TILE_S, y*settings.TILE_S)
                    )

    def load_map_data(self, type, map_id):
        """Returns data representation of map file."""
        try:
            return (Image.open('assets/maps/%s_%s.png' % (type, map_id)).load()
                    if map_id else None)
        except:
            return None
